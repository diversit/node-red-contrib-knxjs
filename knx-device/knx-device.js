/*
  KNX nodes for IBM's Node-Red
  https://bitbucket.org/ekarak/node-red-contrib-knxjs
  (c) 2016, Elias Karakoulakis <elias.karakoulakis@gmail.com>

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.

*/

module.exports = function (RED) {

  var knx = require('knx');
  var util = require('util');

  function EtsCsvExportNode(n) {
    RED.nodes.createNode(this, n);
    this.all = n.all;
  }
  //
  RED.nodes.registerType("ets-csv-export", EtsCsvExportNode);

  // =========================
  function KnxDeviceNode(config) {
  // =========================
    RED.nodes.createNode(this, config);

    var node = this;
    var KNXController = RED.nodes.getNode(config.controller);

    if (this.datapoint) this.datapoint.removeAllListeners();
    if (!KNXController) {
      node.warn("Not bound to a KNX connection");
    }
    var options = {'ga': config.groupaddress, 'dpt': config.dpt };
    if (config.statusga && config.statusga != config.groupaddress)
      options.statusga = config.statusga;
    node.log(util.format('== Initialising datapoint: %j', options));
    this.datapoint = new knx.Datapoint(options, KNXController.connection);
    this.datapoint.on('change', function(oldval, newval) {
      // node.log(util.format('%s changed from %j to %j', node.name, oldval, newval));
      // STATUS group address change => send Node message
      var payload = {'value': newval, 'oldval': oldval};
      if (node.datapoint.dpt.subtype) {
        payload.unit = node.datapoint.dpt.subtype.unit;
        payload.desc = node.datapoint.dpt.subtype.desc;
      }
      node.send({
        'topic': node.name,
        'payload': payload
      });
      node.updateNodeStatus("red", util.format("CHANGE %j => %j", oldval, newval));
    });

    this.updateNodeStatus = function(color, msg) {
      node.status({
        fill: color, shape: "dot", text: msg
      });
      setTimeout(function() {
        node.status({
          fill: "green", shape: "dot", text:  node.datapoint.toString()
        });
      }, 500);
    };

    // incoming message from Node => write value to KNX
    this.on('input', function (msg) {
      if (!config.groupaddress || !config.controller) {
        return null;
      }
      // temporarily update node visual status
      this.updateNodeStatus("red",
        util.format("Sending %j to %s", msg.payload, config.groupaddress)
      );
      // write to our datapoint
      if (node.datapoint) node.datapoint.write(msg.payload.value || msg.payload);
    });

  }
  //
  RED.nodes.registerType("knx-device", KnxDeviceNode);
};
